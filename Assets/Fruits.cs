using System.Collections;
using UnityEngine;

public class Fruits : MonoBehaviour
{
    [SerializeField] private Animator m_animator;
    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.gameObject.CompareTag("Player"))
        {
            m_animator.SetInteger("State", (int)AnimName.Collected);
            Destroy(gameObject,0.5f);
        }
    }
}
