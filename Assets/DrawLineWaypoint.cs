using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLineWaypoint : MonoBehaviour
{
    [SerializeField] List<Transform> m_listWayPoints;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        
        if (m_listWayPoints != null && m_listWayPoints.Count > 1)
        {
            for (int i = 0; i < m_listWayPoints.Count - 1; i++)
            {
                Gizmos.DrawLine(m_listWayPoints[i].position, m_listWayPoints[i + 1].position);
            }
            //Gizmos.DrawLine(m_listWayPoints[m_listWayPoints.Count - 1].position, m_listWayPoints[0].position);
        }
    }
}
