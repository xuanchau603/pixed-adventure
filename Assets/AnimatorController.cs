using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AnimName
{
    Apple = 0,
    Banana = 1,
    Cherry = 2,
    Orange = 3,
    Melon = 4,
    Kiwi = 5,
    Pineapple = 6,
    Strawberry = 7,
    Collected = 100
}

public class AnimatorController : MonoBehaviour
{
    [SerializeField] private Animator m_animator;
    [SerializeField] private AnimName m_animName;

    public Animator Animator => m_animator;

    void Start()
    {
        m_animator.SetInteger("State", (int)m_animName);
    }
}