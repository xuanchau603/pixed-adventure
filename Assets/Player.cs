using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [Header("Move info")] [SerializeField] private float m_moveSpeed;
    [SerializeField] private float m_jumpForce;

    [Header("Collison info")] [SerializeField]
    private Transform m_wallCheck;
    [SerializeField] private LayerMask m_groundLayer;
    [SerializeField] private Transform m_groundCheck;
    [SerializeField] private float m_groundCheckRadius;
    [SerializeField] private LayerMask m_layerWall;
    [SerializeField] private float m_wallCheckRadius;
    [SerializeField] private float m_wallSlideSpeed;


    [Header("Component")] [SerializeField] private Rigidbody2D m_rigid;
    [SerializeField] private Animator m_animator;
    private float dirX;
    private bool m_canDoubleJump = true;
    private bool m_canWallSilde;
    private bool m_isWallSilding;
    private bool m_isDie;

    private enum AnimState
    {
        Idle = 0,
        Running = 1,
        Jump = 2,
        Fall = 3,
        DoubleJump = 4,
        WallSliding = 5,
        Die = 6
    }

    private AnimState m_AnimState = AnimState.Idle;
    private static readonly int State = Animator.StringToHash("State");


    void Update()
    {
        dirX = Input.GetAxis("Horizontal");
        Move();
        Flip();
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            Jump();
        }
        WallSile();
        AnimatorController();
    }


    void WallSile()
    {
        if (IsWalled() && !IsGrounded())
        {
            m_rigid.velocity = new Vector2(m_rigid.velocity.x,
                Mathf.Clamp(m_rigid.velocity.y, -m_wallSlideSpeed, float.MaxValue));
        }
        else
        {
            m_rigid.velocity = new Vector2(m_rigid.velocity.x, m_rigid.velocity.y);
        }
    }

    void Move()
    {
        m_rigid.velocity = new Vector2(dirX * m_moveSpeed, m_rigid.velocity.y);
    }

    void Flip()
    {
        if (dirX > 0)
        {
            transform.rotation = new Quaternion(0, 0, 0, 0);
        }
        else if (dirX < 0)
        {
            transform.rotation = new Quaternion(0, 180, 0, 0);
        }
    }

    void AnimatorController()
    {
        m_AnimState = (dirX == 0 && m_rigid.velocity.y == 0) ? AnimState.Idle : AnimState.Running;
        if (m_rigid.velocity.y > 1 && m_canDoubleJump && !IsGrounded() && !IsWalled())
        {
            m_AnimState = AnimState.Jump;
        }
        else if (m_rigid.velocity.y > 1 && !m_canDoubleJump && !IsGrounded() && !IsWalled())
        {
            m_AnimState = AnimState.DoubleJump;
        }
        else if (m_rigid.velocity.y < 1 && !IsGrounded() && IsWalled())
        {
            m_AnimState = AnimState.WallSliding;
        }
        else if (m_rigid.velocity.y < -1 && !IsWalled())
        {
            m_AnimState = AnimState.Fall;
        }else if (m_isDie)
            m_AnimState = AnimState.Die;
        
        m_animator.SetInteger(State, (int)m_AnimState);
    }

    void Jump()
    {
        if (IsGrounded() || IsWalled())
        {
            m_canDoubleJump = true;
            m_rigid.velocity = new Vector2(m_rigid.velocity.x, m_jumpForce);
        }
        else if (m_canDoubleJump)
        {
            m_rigid.velocity = new Vector2(m_rigid.velocity.x, m_jumpForce);
            m_canDoubleJump = false;
        }
    }


    private void OnCollisionEnter2D(Collision2D target)
    {
        
    }

    private void OnCollisionExit2D(Collision2D target)
    {
        if (target.gameObject.CompareTag("Traps"))
        {
            m_isDie = true;
        }
    }

    void ReStart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    

    bool IsGrounded()
    {
        return Physics2D.OverlapCircle(m_groundCheck.position, m_groundCheckRadius, m_groundLayer);
    }

    bool IsWalled()
    {
        return Physics2D.OverlapCircle(m_wallCheck.position, m_wallCheckRadius, m_layerWall);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(m_groundCheck.position, m_groundCheckRadius);
        Gizmos.DrawWireSphere(m_wallCheck.position, m_wallCheckRadius);
    }
}