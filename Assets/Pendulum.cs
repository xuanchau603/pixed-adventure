using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pendulum : MonoBehaviour
{
    [Header("Prefabs")] [SerializeField] private GameObject m_chain;
    [SerializeField] private GameObject m_spikeBall;


    [SerializeField] private float m_roTime;
    [SerializeField] private float m_roAngle;
    [SerializeField] private float m_roSpeed;


    [SerializeField] private int m_lenghtRope;

    [SerializeField] private bool m_isTwirl;

    private void Awake()
    {
        for (int i = 0; i <= m_lenghtRope; i++)
        {
            if (i < m_lenghtRope)
            {
                var Chain = Instantiate(m_chain, transform);
                Chain.transform.localPosition = new Vector3(0,-i,0);
            }
            else
            {
                m_spikeBall.transform.localPosition = new Vector3(0,-i,0);
            }
        }
    }

    private void Update()
    {
        if (m_isTwirl)
        {
            transform.Rotate(0,0, m_roSpeed * Time.deltaTime);
        }
        else
        {
            m_roTime += Time.deltaTime;
            transform.rotation = Quaternion.Euler(0,0,m_roAngle * Mathf.Sin(m_roTime * m_roSpeed));
        }
        
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, new Vector3(transform.position.x, (transform.position.y - m_lenghtRope), transform.position.z));
    }
}
