using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform m_targetTranform;
    

    // Update is called once per frame
    void LateUpdate()
    {
        var targetPosition = m_targetTranform.position;
        transform.position = new Vector3(targetPosition.x, targetPosition.y, transform.position.z);
    }
}