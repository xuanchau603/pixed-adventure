using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallTrap : MonoBehaviour
{
    [SerializeField] private Rigidbody2D m_rigid;
    [SerializeField] private BoxCollider2D m_collider;
    [SerializeField] private Animator m_animator;
    public float moveDistance = 1.0f;
    private bool isStop;


    void Update()
    {
        if (isStop)
        {
            transform.position = Vector2.MoveTowards(transform.position,
                new Vector2(transform.position.x, transform.position.y - 0.2f), 0.5f);
        }
    }

    private void OnCollisionEnter2D(Collision2D target)
    {
        if (target.gameObject.CompareTag("Player"))
        {
            StartCoroutine(DelayFall(1));
        }
    }

    IEnumerator DelayFall(float seconds)
    {
        m_animator.SetTrigger("Fall");
        yield return new WaitForSeconds(seconds);
        m_rigid.bodyType = RigidbodyType2D.Dynamic;
        m_collider.isTrigger = true;
        Destroy(gameObject, seconds);
    }
}