using UnityEngine;
using UnityEngine.Events;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private KeyCode m_jumpKeyCode;
    [SerializeField] private KeyCode m_moveLeftKeyCode;
    [SerializeField] private KeyCode m_moveRightKeyCode;

    public UnityEvent OnPressJumpeHandler;
    public UnityEvent OnPressMoveHandler;


    private bool m_canInput = true;


    private void Update()
    {
        if (!m_canInput)
            return;
        if (Input.GetKeyDown(m_jumpKeyCode))
        {
            OnPressJumpeHandler?.Invoke();
        }
        if (Input.GetKeyDown(m_moveLeftKeyCode) || Input.GetKeyDown(m_moveRightKeyCode))
        {
            OnPressMoveHandler?.Invoke();
        }
    }

    public void SetInput(bool isCan)
    {
        m_canInput = isCan;
    }
}
