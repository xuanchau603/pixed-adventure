using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    [SerializeField] private float m_jumpForce;
    [SerializeField] private Rigidbody2D m_rigid;

    public void Jumping()
    {
        m_rigid.AddForce(new Vector2(0f, m_jumpForce), ForceMode2D.Impulse);
    }
}