using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class WaypointFollower : MonoBehaviour
{
    [SerializeField] private List<GameObject> m_listWaypoints;
    private int m_currentWaypointIndex = 0;
    [SerializeField] private float m_speed;
    [SerializeField] private bool m_isCyclic;
    private bool m_isBack;

    void Update()
    {
        if (Vector2.Distance(m_listWaypoints[m_currentWaypointIndex].transform.position, transform.position) <= 0.1f)
        {
            
            if (m_isCyclic)
            {
                m_currentWaypointIndex++;
                if (m_currentWaypointIndex >= m_listWaypoints.Count)
                {
                    m_currentWaypointIndex = 0;
                }
            }
            else 
            {
                if (m_isBack)
                {
                    m_currentWaypointIndex--;
                }
                else
                {
                    m_currentWaypointIndex++;
                }
                if (m_currentWaypointIndex >= m_listWaypoints.Count)
                {
                    m_isBack = true;
                    m_currentWaypointIndex--;
                }
                else 
                if (m_currentWaypointIndex <= 0)
                {
                    m_isBack = false;
                }
            }
            
        }
        transform.position =
            Vector2.MoveTowards(transform.position, m_listWaypoints[m_currentWaypointIndex].transform.position,
                Time.deltaTime * m_speed);
    }
    
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if (m_listWaypoints.Count > 1)
        {
            for (int i = 0; i < m_listWaypoints.Count - 1; i++)
            {
                Gizmos.DrawLine(m_listWaypoints[i].transform.position, m_listWaypoints[i + 1].transform.position);
            }
            if(m_isCyclic)
                Gizmos.DrawLine(m_listWaypoints[m_listWaypoints.Count - 1].transform.position, m_listWaypoints[0].transform.position);
        }
    }
}